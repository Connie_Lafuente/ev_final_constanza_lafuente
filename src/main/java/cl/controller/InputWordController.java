/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.controller;

import cl.dao.DictionaryDdbbJpaController;
import cl.entities.DictionaryDdbb;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import utils.Config;



/**
 *
 * @author Constance
 */
@WebServlet(name = "InputWordController", urlPatterns = {"/InputWordController"})
public class InputWordController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet InputRegisterController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet InputRegisterController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String action = request.getParameter("action");

        //Nos lleva a la vista dictionary.jsp
        if (action.equals("searchWord")) {
                       
            request.getRequestDispatcher("dictionary.jsp").forward(request, response);
            return;
        }
        
        if (action.equals("search")) {

            Config.getEnvironmentVariables();
            
            String word = request.getParameter("word");
            Client client = ClientBuilder.newClient();
            WebTarget myResource = client.target(Config.host + "/" + word);
            List<String> definitions = myResource.request(MediaType.APPLICATION_JSON).get(new GenericType<List<String>>(){});
       
            Calendar calendar = Calendar.getInstance();
            Date currentDate = calendar.getTime();
            DictionaryDdbb dictionary = new DictionaryDdbb();
            dictionary.setDicWord(word);
            dictionary.setDicDaytime(currentDate);
            dictionary.setDicCode("" + currentDate.getTime());
       
            DictionaryDdbbJpaController dao = new DictionaryDdbbJpaController();

            try {
                dao.create(dictionary);
            } catch (Exception ex) {
                Logger.getLogger(InputWordController.class.getName()).log(Level.SEVERE, null, ex);
            }

            request.setAttribute("definitions", definitions);
            request.getRequestDispatcher("searchResult.jsp").forward(request, response);
            
            return;
        }

          // Config.getEnvironmentVariables();
                
           Client client = ClientBuilder.newClient();
           WebTarget myResource = client.target(Config.host);
           
            List<DictionaryDdbb> word = myResource.request(MediaType.APPLICATION_JSON).get(new GenericType<List<DictionaryDdbb>>(){});
                      
            request.setAttribute("words", word);    
               //Nos lleva a la vista history.jsp
            request.getRequestDispatcher("history.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
