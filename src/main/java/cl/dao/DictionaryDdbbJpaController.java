/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.dao;

import cl.dao.exceptions.NonexistentEntityException;
import cl.dao.exceptions.PreexistingEntityException;
import cl.entities.DictionaryDdbb;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Constance
 */
public class DictionaryDdbbJpaController implements Serializable {
    
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("dictionaryDS");
    
    public DictionaryDdbbJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    
     public DictionaryDdbbJpaController() {
 
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(DictionaryDdbb dictionaryDdbb) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(dictionaryDdbb);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findDictionaryDdbb(dictionaryDdbb.getDicCode()) != null) {
                throw new PreexistingEntityException("DictionaryDdbb " + dictionaryDdbb + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(DictionaryDdbb dictionaryDdbb) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            dictionaryDdbb = em.merge(dictionaryDdbb);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = dictionaryDdbb.getDicCode();
                if (findDictionaryDdbb(id) == null) {
                    throw new NonexistentEntityException("The dictionaryDdbb with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DictionaryDdbb dictionaryDdbb;
            try {
                dictionaryDdbb = em.getReference(DictionaryDdbb.class, id);
                dictionaryDdbb.getDicCode();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The dictionaryDdbb with id " + id + " no longer exists.", enfe);
            }
            em.remove(dictionaryDdbb);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<DictionaryDdbb> findDictionaryDdbbEntities() {
        return findDictionaryDdbbEntities(true, -1, -1);
    }

    public List<DictionaryDdbb> findDictionaryDdbbEntities(int maxResults, int firstResult) {
        return findDictionaryDdbbEntities(false, maxResults, firstResult);
    }

    private List<DictionaryDdbb> findDictionaryDdbbEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(DictionaryDdbb.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public DictionaryDdbb findDictionaryDdbb(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(DictionaryDdbb.class, id);
        } finally {
            em.close();
        }
    }

    public int getDictionaryDdbbCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<DictionaryDdbb> rt = cq.from(DictionaryDdbb.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
