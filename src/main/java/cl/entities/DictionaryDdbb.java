/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Constance
 */
@Entity
@Table(name = "dictionary_ddbb")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DictionaryDdbb.findAll", query = "SELECT d FROM DictionaryDdbb d"),
    @NamedQuery(name = "DictionaryDdbb.findByDicCode", query = "SELECT d FROM DictionaryDdbb d WHERE d.dicCode = :dicCode"),
    @NamedQuery(name = "DictionaryDdbb.findByDicWord", query = "SELECT d FROM DictionaryDdbb d WHERE d.dicWord = :dicWord"),
    @NamedQuery(name = "DictionaryDdbb.findByDicDaytime", query = "SELECT d FROM DictionaryDdbb d WHERE d.dicDaytime = :dicDaytime")})
public class DictionaryDdbb implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "dic_code")
    private String dicCode;
    @Size(max = 2147483647)
    @Column(name = "dic_word")
    private String dicWord;
    @Column(name = "dic_daytime")
    @Temporal(TemporalType.TIME)
    private Date dicDaytime;

    public DictionaryDdbb() {
    }

    public DictionaryDdbb(String dicCode) {
        this.dicCode = dicCode;
    }

    public String getDicCode() {
        return dicCode;
    }

    public void setDicCode(String dicCode) {
        this.dicCode = dicCode;
    }

    public String getDicWord() {
        return dicWord;
    }

    public void setDicWord(String dicWord) {
        this.dicWord = dicWord;
    }

    public Date getDicDaytime() {
        return dicDaytime;
    }

    public void setDicDaytime(Date dicDaytime) {
        this.dicDaytime = dicDaytime;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dicCode != null ? dicCode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DictionaryDdbb)) {
            return false;
        }
        DictionaryDdbb other = (DictionaryDdbb) object;
        if ((this.dicCode == null && other.dicCode != null) || (this.dicCode != null && !this.dicCode.equals(other.dicCode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.entities.DictionaryDdbb[ dicCode=" + dicCode + " ]";
    }
    
}
