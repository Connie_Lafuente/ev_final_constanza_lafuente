/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.services;

import cl.dao.DictionaryDdbbJpaController;
import cl.entities.DictionaryDdbb;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import utils.Config;
import root.services.*;

/**
 *
 * @author Constance
 */
@Path("/diccionario")
public class DictionaryInnerRest {

    EntityManagerFactory emf = Persistence.createEntityManagerFactory("dictionaryDS");
    EntityManager em;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listEverything() {

        DictionaryDdbbJpaController dao = new DictionaryDdbbJpaController();
        List<DictionaryDdbb> list = dao.findDictionaryDdbbEntities();

        return Response.ok(200).entity(list).build();
    }

    /*
    OkHttpClient client = new OkHttpClient().newBuilder()
        .build();
        Request request = new Request.Builder()
        .url("https://od-api.oxforddictionaries.com/api/v2/entries/es/ejemplo?fields=definitions")
        .method("GET", null)
    .addHeader("app_id", "026516df")
        .addHeader("app_key", "fe45bd556a8d8b835a5a3409fb079955")
        .build();
        Response response = client.newCall(request).execute();
     */
    // Para más tarde con la API de Oxford
    @GET
    @Path("/{palabra}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchWord(@PathParam("palabra") String palabra) {
        Client client = ClientBuilder.newClient();
        
        WebTarget myResource = client.target("https://od-api.oxforddictionaries.com/api/v2/entries/es/" + palabra + "?fields=definitions");
        
        WordDefinition result = myResource
                .request(MediaType.APPLICATION_JSON)
                .header("app_id", "026516df")
                .header("app_key", "fe45bd556a8d8b835a5a3409fb079955")
                .get(WordDefinition.class);
        
        List<String> definitions = new ArrayList<>();
     
        List<Result> results = result.getResults();
        for(Result r : results) {
            for (LexicalEntry lexicalEntry: r.getLexicalEntries()) {
                for (Entry entry : lexicalEntry.getEntries()) {
                    for (Sense sense : entry.getSenses()) {
                        for(String definition: sense.getDefinitions()) {
                            definitions.add(definition);
                        }
                    }
                }
            }
        }
        
        return Response.ok(200).entity(definitions).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public String newWord(DictionaryDdbb word) {
        DictionaryDdbbJpaController dao = new DictionaryDdbbJpaController();
        try {
            dao.create(word);
        } catch (Exception ex) {
            Logger.getLogger(DictionaryDdbb.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Palabra Guardada";
    }
}
