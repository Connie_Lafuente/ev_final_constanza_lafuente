<%-- 
    Document   : dictionary
    Created on : 01-nov-2020, 18:55:23
    Author     : Constance
--%>

<html style="height: 100vh">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet"
              href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <title>Buscador Palabras</title>
    </head>
    <body>

        <div class="d-flex flex-column align-items-center justify-content-center" style="height: 60vh">
            <div class="d-flex align-self-center my-5 mx-5">
                <h1>Busca la Definición de una Palabra</h1>

                <div class="container my-5">
                    <form name="form" method="POST" action="InputWordController">
                        <div class="form-group">
                            <label for="word">Ingresa Palabra</label>
                            <input type="text" name="word" class="form-control" required id="word" >
                        </div>
                        <button type="submit" name="action" value="search" class="btn btn-lg btn-success">Buscar</button>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>

