<%-- 
    Document   : history
    Created on : 01-nov-2020, 15:24:10
    Author     : Constance
--%>

<%@page import="cl.entities.DictionaryDdbb"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<DictionaryDdbb> words = (List<DictionaryDdbb>) request.getAttribute("words");
    Iterator<DictionaryDdbb> itDictionaryDdbb = words.iterator();
%>
<html style="height: 100vh">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet"
              href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <title>Historial de Búsqueda</title>
    </head>
    <body class="text-center">
        <h1 class="mt-4">Bienvenidos al Historial de Búsqueda</h1>
        <div class="d-flex justify-content-center">

            <div class="d-flex flex-column justify-content-center" style="height: 80vh">
                <table class="table table-hover">
                    <thead>
                    <th>Código</th>
                    <th>Palabra Buscada</th>
                    <th>Fecha de la Consulta</th>
                    </thead>
                    <tbody>
                        <%while (itDictionaryDdbb.hasNext()) {
                                    DictionaryDdbb word = itDictionaryDdbb.next();%>
                        <tr>
                            <td><%= word.getDicWord()%></td>
                            <td><%= word.getDicDaytime()%></td>
                        </tr>
                        <%}%>                
                    </tbody>           
                </table>    
            </div>             
    </body>
</html>
