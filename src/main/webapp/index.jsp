<%-- 
    Document   : index
    Created on : 31-oct-2020, 16:31:23
    Author     : Constance
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html style="height: 100vh">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet"
         href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <title>Pet Register</title>
    </head>
    <body>
        <div class="container mt-4">
            <h3>Constanza Lafuente M.</h3>
            <br>
            <h4>Sección 50</h4>
            <br>
            <h4>https://bitbucket.org/Connie_Lafuente/ev_final_constanza_lafuente/src/master/</h4>
            <h4>https://eva-final-constanza-lafuente.herokuapp.com/</h4>
        </div>
     
        
        <div class="d-flex flex-column align-items-center justify-content-center" style="height: 60vh">
            <div class="d-flex align-self-center my-5 mx-5">
                <h1>Bienvenidos a Busca Palabra</h1>
             
                <div class="container my-5">
                    <form name="form" method="POST" action="InputWordController" >

                        <button type="submit" name="action" value="searchWord" class="btn btn-lg btn-success">Buscar Palabra</button>
                        <button type="submit" name="action" value="history" class="btn btn-lg btn-success">Historial</button>

                    </form>
                </div>
            </div>
        </div>
    </body>
</html>

